module.exports = {
  thirdParty: {
    'js/vendors/jquery.min.js': './src/js/jquery.js',
    'css/main.min.css': './src/scss/style.scss',
    'css/vendors/bootstrap.min.css': './src/scss/_bootstrap.scss',
    'css/vendors/mdb.min.css': './src/scss/_mdb.scss',
    'css/icons/material-icons.min.css': './src/scss/icons/_material-icons.scss',
    'css/components/components.min.css': './src/scss/_components.scss',
    'js/polyfill.min.js': ['babel-polyfill'],
    'js/main.min.js': ['./src/js/bootstrap.js', './src/js/jquery.js'],
    'js/vendors/bootstrap.min.js': './src/js/bootstrap.js',
    'js/vendors/mdb.min.js': './src/js/vendors/mdb/mdb.js',
  },
  mapObj: {
    'src/': "",
    'scss': "css",
    '_': "",
    '.js': ".min.js",
    '.scss': ".min.css",
  },
}