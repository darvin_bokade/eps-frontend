const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const glob = require('glob');
const compile = require('./compile');
// const CopyWebpackPlugin = require('copy-webpack-plugin');

const extractPlugin = new ExtractTextPlugin({
  filename: '[name]',
});

function getEntryPoints() {
  let buildFiles = glob.sync("src/*/pages/**/*.*[js|scss]");
  buildFiles.forEach(file => {
    let tempFile = file.replace(new RegExp('(src\/)|(scss)|(_)|(.js)|(.scss)', 'g'), function (matched) {
      return compile.mapObj[matched];
    });
    compile.thirdParty[tempFile] = './' + file;
  });
  return compile.thirdParty;
}

module.exports = {
  entry: getEntryPoints(),
  output: {
    filename: '[name]',
    chunkFilename: "chunk-[id]/[chunkhash:8].chunk.js",
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js?$/,
        exclude: [/node_modules/, /vendors/],
        loader: 'eslint-loader',
        options: {
          fix: true,
        },
      },
      {
        test: /\.js?$/,
        exclude: [/node_modules/, /vendors/],
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env', 'stage-0'],
            },
          },
        ],
      },
      {
        test: /\.scss?$/,
        use: extractPlugin.extract({
          use: [
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                outputStyle: 'compressed',
                sourceMap: false,
                data: '@import "./src/scss/util.scss";',
                includePaths: [
                  path.join(__dirname, 'src')
                ],
              }
            }
          ],
        }),
      },
      {
        test: /\.html$/,
        use: ['html-loader'],
      },
      // Fonts
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        exclude: [/vendors/, /img/, /icons/],
        loader: 'file-loader?name=font/[name].[ext]',
      },
      // Material icons
      {
        test: /MaterialIcons-Regular.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        exclude: [/node_modules/, /img/],
        loader: 'file-loader',
        options: {
          name: 'icons/[name].[ext]',
          publicPath: '../../',
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          useRelativePath: true,
        },
      },
    ],
  },
  plugins: [
    extractPlugin,
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
      'window.jQuery': 'jquery',
      // Waves: 'node-waves',
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
    }),
    // new CopyWebpackPlugin([{ from: 'src/font', to: 'font' }, { from: 'src/img', to: 'img' }]),
    new CleanWebpackPlugin(['dist']),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        js: {
          test: /\.js$/,
          name: "main",
          chunks: "all",
          minChunks: 7,
        },
        css: {
          test: /\.(css|sass|scss)$/,
          name: "main",
          chunks: "all",
          minChunks: 2,
        }
      }
    }
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
  devtool: 'source-map',
  target: 'web',
};