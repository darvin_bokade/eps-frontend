# MDB + Webpack Starter Kit EPS

A Material Design for Bootstrap boilerplate for Webpack 4.

#### Getting Started

This project contains everything you need to get setup with the latest version of MDB. Simply follow the instructions under and you're ready to go in seconds.

1.  Clone the repository

```bash
# Navigate to the folder you want to download the project into and run
git clone https://darvin_bokade@bitbucket.org/darvin_bokade/eps-frontend.git
```

2.  **Install dependencies**

```bash
# Installs all development and production dependencies
npm install
```

4)  **Start developing**

```bash
# Start the development server
# http://localhost:8080 - loads automatically
npm run dev

# Build a production version of the project
npm run build
```

&nbsp;

#### Project workflow

ES6 ready environment
**Tools**

* Babel
* Webpack 4
* Eslint
* Airbnb style guide

**Libraries**

* jQuery
* Bootstrap 4
* Material Design for Bootstrap

---

## Special Thanks

The developers of Sooryen
